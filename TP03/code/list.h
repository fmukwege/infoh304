#include "noeud.h"
#include <fstream>
//#include <ostream>
class Liste
{
private:
	int nombreNoeuds;
	Noeud* tete;
	//int compteur;
public:
	Liste();
	~Liste();
	
	void insere(string chaine);
	void insereTrie(string chaine);
	void supprime();
	void imprimeListe() const;
	void imprimeListe(std::ostream &  out) const;
	int getNombreNoeuds() const;
	Noeud* getTete() const;


};
