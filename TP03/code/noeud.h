#include <iostream>
#include <string>
using std::string;

class Noeud
{
private:
	string data;
	Noeud* suivant;
	int compteur;
public:
	Noeud();
	Noeud(string chaine);
	void setSuivant(Noeud* n);
	Noeud* getSuivant() const;
	string getData() const;
	int getCompteur() const;
	void incrementCompteur();
	
	// const Noeud* & operator= (const Noeud* & NoeudOther){
    // if (this != NoeudOther){
    //     data  = (NoeudOther->getData());
	// 	suivant = NoeudOther->getSuivant();
    // }
	// return this;
	// };
};
