#include "noeud.h"
//Ici je definis les fonctions de la classe Noeud definie dans le header éponyme
//Pour ce faire, je dois mettre Class:: devant afin d'y accéder. ou class est le nom de la classe
Noeud::Noeud() {
    data ="";
    compteur = 0;
    suivant = NULL;};
Noeud::Noeud(string chaine = "")  : data(chaine) {
    suivant =NULL;
};
void Noeud::setSuivant(Noeud* n){
    suivant = n;
};
Noeud* Noeud::getSuivant() const{return suivant;};
string Noeud::getData() const{return data;};
int Noeud::getCompteur() const{return compteur;}
void Noeud::incrementCompteur(){compteur++;}