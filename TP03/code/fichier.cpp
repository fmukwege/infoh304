#include <iostream>
#include <string>
#include <fstream>
using std::cout;
using std::endl;
using std::string;
using std::ifstream;

int main(int argc, char *argv[])
{
	ifstream fichier ("./test.txt");
	if( fichier.is_open() )
	{
		string mot;
		while( fichier >> mot )
			cout << "Mot courant: " << mot << endl;
		fichier.close();
	}
	else
		cout << "Impossible d'ouvrir le fichier" << endl;
	return 0;
}