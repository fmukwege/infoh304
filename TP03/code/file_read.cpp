#include "list.h"
#include <fstream>


int main(int argc, char* argv[]){

  std::ifstream file(argv[1]);
  string word;
  Liste* list = new Liste();
  std::cout << "the file target is " << argv[1] << std::endl;
  if (file.is_open())
  {
      while (file >> word){
          list->insere(word);
          std::cout << "The word added is : "<<word<< std::endl;
          std::cout << "How many nodes ?  : "<<(list->getNombreNoeuds())<< std::endl;
      }
      //list->imprimeListe();
  }
  else{
      std::cout <<"File not opened"<< std::endl;
  }
  delete list;
  file.close();
  std::cout <<"-----DONE-------"<< std::endl;
  return 0;   
}