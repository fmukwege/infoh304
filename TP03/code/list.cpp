#include "list.h"

Liste::Liste() {
    nombreNoeuds =0;
    tete = NULL;
    
};
Liste::~Liste(){ //supprimer toutes les têtes
    while (tete != NULL){
        supprime();
    }
};
void Liste::insere(string chaine){
    
    Noeud* next  =  new Noeud(chaine);
    next->setSuivant(tete);
    tete = next;
    nombreNoeuds++;
};
void Liste::supprime(){
    if(tete == NULL){
        std::cout<< "Empty list" << std::endl;
    }
    else{
        Noeud* temp = tete;
        tete = tete->getSuivant(); 
        delete temp; //good practice, never access a freed memory.
        nombreNoeuds--;
        std::cout<< "------Head deleted-------" << std::endl;
    }

};
// void Liste::imprimeListe() const{
//     int i =0;
//     Noeud* temp = tete;
//     while (i != nombreNoeuds){
//         std::cout<<"We are at the node number " << i << std::endl;
//         std::cout<<"and it stores the string "<< temp->getData() << std::endl;   
//         temp = temp->getSuivant();
//         i++;
//     }
//     std::cout<<"----------DONE-------------"<< std::endl;
// };
void Liste::imprimeListe(std::ostream & out= std::cout) const{
    int i =0;
    out<<"----occurences in text" << "---------"<< std::endl;
    Noeud* temp = getTete();
    while (i != getNombreNoeuds()){
        out << temp->getData() << " : " << temp->getCompteur()+1 << std::endl;
        temp = temp->getSuivant();
        i++;
    }
    std::cout<<"----------DONE-------------"<< std::endl;
};
int Liste::getNombreNoeuds() const{return nombreNoeuds;};
Noeud* Liste::getTete() const{return tete;};

/* Beware the operator< compare the ASCII number of the first char in the string
So capital letters come first ... Do not get tricked */
void Liste::insereTrie(string chaine){
    //cout << "-----------InsereTrie-----------"<< endl;
    int i = 0;
    
    //Think wisely to all cases and try to resume them into ifelse statements
    // condition are evaluated from a to b so use it at your advantage
    // a OR b , thus a is evaluated before b
    if ((tete == NULL) || (tete->getData()> chaine )) //Insert if empty or in front
    {
        insere(chaine);
        //std::cout << "-----Sorted and added-----  : " << chaine << std::endl;
    }
    else{
        Noeud* temp = tete;   //have to be created there because it needs to point smth else that NULL
        //insert at the end of the list if you don't stop where you have a valid pos 
        while((temp->getSuivant() !=NULL) && ((temp->getSuivant()->getData()) <= chaine)){
            //cout << "----- Moving -----" << endl;
            temp = temp->getSuivant();
            i++;
        }
        if (temp->getData() != chaine){
            //cout << "----- Valid position ----- : "<< i << endl;
            Noeud* middle = new Noeud(chaine);
            middle->setSuivant(temp->getSuivant()); //link the chain to the new node from node to end
            temp->setSuivant(middle); // Link the chain to the node from beginning to node
            nombreNoeuds++;
        }
        else{ // if already in the list 
            temp->incrementCompteur();
            //std::cout << (temp->getCompteur()+1) << " occurences from the word : " << chaine << std::endl;
        }
        
        //cout << "----- Moving to the next one -----" << endl;
    }
    //cout << "***** Ended insereTrie *****"<< endl;
}
