#include <string.h>
#include <stdio.h>
#include "list.h"
//#include <cstdio.h>
#define LONGMAX 50
void fimprimer_liste(noeud* tete, FILE* file){

    if (tete == NULL)
    {
        printf("Empty list ! \n");
    }
    else{
        while(tete != NULL){
            fprintf(file,"%s \n",tete->data);
            //fprintf("Element %i is : %s \n",n,tete->data);
            tete = tete->suivant;        
        };
    }
}
int main(int argc, char* argv[]){
    FILE* book = NULL;
    FILE* book_inv = NULL;
    noeud* liste = NULL;
    int control = 0;
    char name[LONGMAX];
    
    printf("The file to be analysed is %s   \n",argv[1]);
    book = fopen(argv[1],"r");
    book_inv = fopen(argv[2],"w+");
    while( ((fscanf(book,"%s",name))!= EOF) ){
        if (!control)
        {
            liste = creer_noeud(name);
        }
        else{
            liste = inserer_noeud(name,liste);
        }
        ++control;
        //fprintf(book_inv,"%s",name);
        
        //printf("Start of the book \n");
        //printf("%s ",name);
    }
    fclose(book);
    fimprimer_liste(liste, book_inv);
    fclose(book_inv);

    //imprimer_liste(liste);
    return 0;
}