#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct noeud_t
{
	char* data;
	struct noeud_t* suivant;
} noeud;

/* creer_noeud: renvoie un pointeur vers un nouveau noeud avec la valeur passée en paramètre */
noeud* creer_noeud(char* data)
{
	noeud* nouveau_noeud = malloc(sizeof(noeud));
    nouveau_noeud->data = malloc(strlen(data)+1);
	strcpy(nouveau_noeud->data,data);
	nouveau_noeud->suivant = NULL;
	return nouveau_noeud;
}

/* inserer_noeud: insère un noeud avec la valeur passée en paramètre */
noeud* inserer_noeud(char* data, noeud* tete)
{
	noeud* nouvelle_tete = creer_noeud(data);
	nouvelle_tete->suivant = tete;
	return nouvelle_tete;
}

/* supprimer_tete: supprimer le noeud de tête */
noeud* supprimer_tete(noeud* tete)
{
	if ( tete == NULL )
		return NULL;
	else
	{
		noeud* nouvelle_tete = tete->suivant;
        free(tete->data);//il faut libérer la mémoire de la plus éloignée (donc la plus vieille) à la plus proche (donc plus récente)
		free(tete);
		return nouvelle_tete;
	}
}
/*Affiche la liste chainée à l'écran*/
void imprimer_liste(noeud* tete){

    int n = 0;
    if (tete == NULL)
    {
        printf("Empty list ! \n");
    }
    else{
        printf("Voici les éléments de la liste chainée : \n");
        while(tete != NULL){
            n++;
            printf("Element %i is : %s \n",n,tete->data);
            tete = tete->suivant;        
        };
    }
    
}

