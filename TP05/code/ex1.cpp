#include <iostream>
#include <string>
#include <set>
#include <fstream>
using namespace std;
template <typename Container>
void print(const Container &c, std::ostream & out= cout) {
    typename Container::const_iterator itr;
    for (itr = c.begin(); itr!= c.end(); ++itr)
    {
        out << itr << " : "<< *itr << endl;
    }
    out<<"----------PRINT DONE-------------"<< endl;
};
// D'abord comptez toutes les occurences et les mettre dans la pair<set,bool>
// then print dans la console ssi la value est true 
// Correction : not even necessary since set.insert() won't add the word if already in the
int main(int argc, char *argv[])
{
    cout <<"---------Ex1 : beginning of the main ------------" << endl;
    pair<set<string>::iterator,bool> unique;
	set<string> words;
    string word;
    int i  =0;
    ifstream file(argv[1]);
    while (file >> word){
        unique = words.insert(word);   
        if (unique.second)
        {
            cout << *(unique.first) << endl;
            i++;
        }
        else{
            //cout << *(unique.first) << " : " << " is already in the set"  << endl;
        }
    }
    
    cout <<"Everything is stored" << endl;
    cout <<"Unique occurences : " << i << endl;    
    // for (unique.first; unique.first  != (words).end(); ++(unique.first))
    // {
    //     cout << *(unique.first) << endl;
    // }
    cout<<"----------PRINT DONE-------------"<< endl;
    return 0;
}