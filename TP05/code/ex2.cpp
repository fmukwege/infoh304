#include <iostream>
#include <string>
#include <set>
#include <map>
#include <fstream>
using namespace std;

void print(const map<string,int> &c, std::ostream & out= cout) {
    map<string,int>::const_iterator itr;
    for (itr = c.begin(); itr!= c.end(); ++itr)
    {
       out  << itr->first << " , " << itr->second  << endl;
    }
    out<<"----------PRINT DONE-------------"<< endl;
};
// D'abord comptez toutes les occurences et les mettre dans la pair<set,bool>
// then print dans la console ssi la value est true 
// Correction : not even necessary since set.insert() won't add the word if already in the set
int main(int argc, char *argv[])
{
    cout <<"---------Ex2 : beginning of the main ------------" << endl;
    pair<map<string,int>::iterator,bool> unique;
	map<string,int> dict;
    string word;
    ofstream  output(argv[2]);
    //int i  =0;
    ifstream file(argv[1]);
    if(file.is_open()){
        while (file >> word){
        ///unique = dict.insert(make_pair(word,1));   
        if (!((dict.insert(make_pair(word,1))).second))
        {
            //if here, output was false so word already inside
            dict[word]++;
        }
        else{
            //nothing to do, first addition of the word to the tree
        }
    }
    }
    else{
        cout << "File not open" << endl;
    }
    if (output.is_open())
    {
        print(dict,output);
    }
    else{
        cout << "Output file not opened"<< endl;
    }

    file.close();
    output.close();;
    
    cout <<"Everything is stored" << endl;   
    // for (unique.first; unique.first  != (words).end(); ++(unique.first))
    // {
    //     cout << *(unique.first) << endl;
    // }
    return 0;
}