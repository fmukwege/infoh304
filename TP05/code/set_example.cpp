#include <iostream>
#include <string>
#include <set>
using namespace std;

int main(int argc, char *argv[])
{
	pair<set<string>::iterator,bool> ret;//pair de clé-value ou la clé est l'iterateur d'un set de string et la valeur est le bool de retour d'une tentative d'ajout du string dans le set 
	set<string> ensemble;
	//On essaye d'insérer le mot "test".
	ret=ensemble.insert("test");
	//apres l'insertion, ret vaut (itérateur vers "test",true).
	cout << "ret.first pointe vers le mot: " << *(ret.first) << endl;
	cout << "ret.second vaut: " << ret.second << endl;
	//On essaye de réinsérer le mot "test".
	ret=ensemble.insert("test");
	//Cette fois, ret vaut (itérateur vers "test",false). pcq on a tjrs le test mais le false est la pcq l'ajout de "test" a nouveau a fail pcq deja présent
	cout << "ret.first pointe vers le mot: " << *(ret.first) << endl;
	cout << "ret.second vaut: " << ret.second << endl;
}