#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

// Tableau de valeurs de pièces
vector<int> values = {1, 3, 4, 10};
// Nombre de valeurs de pièces
int n = values.size();

int ntot(int m)
{
	//Renvoie le nombre de pièces nécessaires pour rendre le montant m
	// A compléter
}

int main(int argc, const char *argv[])
{
    //Renvoie le nombre de pièces nécessaires pour rendre le montant m passé en paramètre sur la ligne de commande
    if (argc == 1) {
        cout << "Entrez une valeur" << endl;
    }
    else {
        // On transforme le paramètre passé sur la ligne de commande en entier
        int m = atoi(argv[1]);
        int best = ntot(m);
        if (best >= INFINITY) {
            cout << "Pas moyen de rendre un montant " << m << "." << endl;
        }
        else {
            cout << "Pour rendre le montant " << m << " il faut " << best << " pieces." << endl;
        }
    }
    return 0;
}
