#include "../../TP03/code/list.h"
//#include <fstream>
using std::cout;
using std::endl;

// the function has to be put back to list.cpp NOT HERE 

int main(int argc, char* argv[]){

  std::ifstream file(argv[1]);
  std::ofstream output(argv[2]);
  string word;
  Liste* list = new Liste();
  std::cout << "the file target is " << argv[1] << std::endl;
  if (file.is_open())
  {
      while (file >> word){
          list->insereTrie(word);
          //std::cout << "The word added is : "<<word<< std::endl;
          //std::cout << "How many nodes ?  : "<<(list->getNombreNoeuds())<< std::endl;
          
      }
      //list->imprimeListe();
  }
  else{
      std::cout <<"File not opened"<< std::endl;
  }
  list->imprimeListe(output);
  delete list;
  file.close();
  output.close();
  std::cout <<"-----MAIN DONE-------"<< std::endl;
  return 0;   
}