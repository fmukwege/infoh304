# Exercises from the Data structures and Algorithms course at ULB - taught in 3rd year bachelor software engineering by Professor Jeremie Roland

forked from jeremieroland/infoh304.git
Read more about the course outcomes here (or down below): https://www.ulb.be/en/programme/info-h304

# Course content:
  
C language:

- Syntax and semantics

- Manual memory allocation and deallocation; pointers

C++ language:

- Syntax and semantics, object oriented programming aspects

- Templates and C++ Standard Template Library (STL)

Basic analysis of algorithms:

- Big-O, Omega and Theta notations

- Notions of algorithm and complexity

- Main complexity classes

Data structures:

- General principles

- Linked lists

- Priority queues, heaps

- Binary search trees

- Hash tables

Sorting algorithms:

- Insertion sort

- Mergesort

- Heapsort

- Quicksort

- Linear sort: counting sort and Radix sort

Algorithmic strategies:

- Divide-and-conquer

- Backtracking

- Dynamic programming

- Greedy algorithms

Objectives (and/or specific learning outcomes)
C and C++ programming:

- to be able to write simple programs in C/C++

- to understand and to use the basic principles of dynamic memory allocation

Algorithms and data structures:

- to be able to design algorithms for simple problems using the basic algorithmic techniques (divide-and-conquer, backtracking, dynamic programming, etc.)

- to be able to design and use elementary data structures adapted to these problems (arrays, linked lists, hash tables, binary search trees, etc.)

- to be able to analyze the complexity of these algorithms

