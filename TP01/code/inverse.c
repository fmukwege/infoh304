#include <string.h>
/* inverse: inverse le sens du mot */
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )// initialisation of 2 counters to read backwards and front
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}