#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONGMAX 30

int main(){
    /*Exercice 1 + 2*/
    int diff = ('a'-'A');
    printf("Exo 1 + 2 \n");
    char word[LONGMAX];
    printf("Please enter a word of maximum %i letters \n",LONGMAX);
    scanf("%s",word);
    
    printf("The word entered is %s\n",word);
    for(int i=0; i<strlen(word);i++ ){
        if((word[i] <= 90) && (word[i] >= 65 )){//If capital letter
            word[i] = word[i] + diff; 
        }
        else{
            word[i] = word[i] - diff; 
        }
    }
    printf("Now my word is like this : %s \n",word);

    return 0;
}