#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONGMAX 30
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )// initialisation of 2 counters to read backwards and front
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}

int main(){
    /*Exercice 1 + 2*/
    printf("Exo 1 + 2 \n");
    char word[LONGMAX];
    //char word_inv[LONGMAX];
    printf("Please enter a word of maximum %i letters \n",LONGMAX);
    scanf("%s",word);
    
    printf("The word entered is %s,\nits length is %lu \n",word,strlen(word));
    inverse(word);
    printf("Its inverse is %s \n",word);




    return 0;
}



/*Exercice 2*/
